package com.bookstore.service;

import com.bookstore.domain.Book;
import com.bookstore.domain.CartItem;
import com.bookstore.domain.ShoppingCart;
import com.bookstore.domain.User;

import java.util.List;

/**
 * Created by ariel, Segunda-feira~>03/07/2017.
 */
public interface CartItemService {
    List<CartItem> findByShoppingCart(ShoppingCart shoppingCart);
    
    CartItem updateCartItem(CartItem cartItem);
    
    CartItem addBookToCartItem(Book book, User user, int qty);
    
    CartItem findById(Long cartItemId);
    
    void removeCartItem(CartItem cartItem);
    
    CartItem save(CartItem cartItem);
}

