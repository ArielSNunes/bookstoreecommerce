package com.bookstore.service;

import com.bookstore.domain.*;

/**
 * Created by ariel, Quarta-feira~>26/07/2017.
 */
public interface OrderService {
    public Order createOrder(
            ShoppingCart shoppingCart, ShippingAddress shippingAddress,
            BillingAddress billingAddress, Payment payment,
            String shippingMethod, User user);
    
    Order findOne(Long id);
}
