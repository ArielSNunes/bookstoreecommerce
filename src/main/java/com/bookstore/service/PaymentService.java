package com.bookstore.service;

import com.bookstore.domain.Payment;
import com.bookstore.domain.UserPayment;

/**
 * Created by ariel, Segunda-feira~>24/07/2017.
 */
public interface PaymentService {
    Payment setByUserPayment(UserPayment userPayment, Payment payment);
}

