package com.bookstore.service;

import com.bookstore.domain.UserShipping;

/**
 * Created by ariel, Terça-feira~>27/06/2017.
 */
public interface UserShippingService {
    UserShipping findById(Long id);
    
    void removeById(Long userShippingId);
}
