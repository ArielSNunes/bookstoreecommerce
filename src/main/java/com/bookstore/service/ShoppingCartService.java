package com.bookstore.service;

import com.bookstore.domain.ShoppingCart;

/**
 * Created by ariel, Segunda-feira~>03/07/2017.
 */
public interface ShoppingCartService {
    ShoppingCart updateShoppingCart(ShoppingCart shoppingCart);
    
    void clearShoppingCart(ShoppingCart shoppingCart);
}
