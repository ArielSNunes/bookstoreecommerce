package com.bookstore.repository;

import com.bookstore.domain.UserShipping;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by ariel, Terça-feira~>27/06/2017.
 */
public interface UserShippingRepository extends CrudRepository<UserShipping, Long> {
}
