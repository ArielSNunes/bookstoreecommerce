package com.bookstore.repository;

import com.bookstore.domain.Order;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by ariel, Quarta-feira~>26/07/2017.
 */
public interface OrderRepository extends CrudRepository<Order, Long> {
}
