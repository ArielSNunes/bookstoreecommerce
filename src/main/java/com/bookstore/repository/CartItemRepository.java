package com.bookstore.repository;

import com.bookstore.domain.CartItem;
import com.bookstore.domain.ShoppingCart;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by ariel, Segunda-feira~>03/07/2017.
 */
@Transactional
public interface CartItemRepository extends CrudRepository<CartItem, Long> {
    List<CartItem> findByShoppingCart(ShoppingCart shoppingCart);
}
