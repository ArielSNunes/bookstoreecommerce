package com.bookstore.repository;

import com.bookstore.domain.ShoppingCart;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by ariel, Segunda-feira~>03/07/2017.
 */
public interface ShoppingCartRepository extends CrudRepository<ShoppingCart, Long> {

}
