package com.bookstore.utility;

import com.bookstore.domain.Order;
import com.bookstore.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.internet.InternetAddress;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Component
public class MailConstructor {
    @Autowired
    private Environment env;
    
    @Autowired
    private TemplateEngine templateEngine;
    
    public SimpleMailMessage constructResetTokenEmail(
            String contextPath, Locale locale, String token,
            User user, String password) {
        String url = contextPath + "/newUser?token=" + token;
        
        String message = "\nPlease click on this link to verify your email and edit your personal information. Your password is: \n"
                + password;
        
        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(user.getEmail());
        email.setSubject("Le's Bookstore - New User");
        email.setText(url + message);
        email.setFrom(env.getProperty("support.email"));
        
        return email;
    }
    
    public MimeMessagePreparator constructOrderConfirmationEmail(User user, Order order, Locale english) {
        // Thymeleaf Context
        Context context = new Context();
        Map<String, Object> mapaDeVariaveis = new HashMap<>();
        mapaDeVariaveis.put("order", order);
        mapaDeVariaveis.put("user", user);
        mapaDeVariaveis.put("cartItemList", order.getCartItemList());
        String text = templateEngine.process("orderConfirmationEmailTemplate", context);
        context.setVariables(mapaDeVariaveis);
        
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper email = new MimeMessageHelper(mimeMessage);
            email.setTo(user.getEmail());
            email.setSubject("Order Confirmation: " + order.getId());
            email.setText(text, true);
            email.setFrom(new InternetAddress("arielsn1.dev@gmail.com"));
        };
        return messagePreparator;
    }
}
